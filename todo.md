# Todo

1. Create a Vue component to handle the S8 logo:
    * Calculate size with different resolutions
    * Set a minimum size
    * Handle the case when the screen is very small
2. Create ad hoc background
3. Create navbar
4. Create animations from the main block to the second
5. Create second block
    * On the left there should be a sticky menu with an icon that rappresent the chapter (e.g. ideal, creators, mantra, etc.)
6. Add the text for the chapters
7. Add restricted area
8. Implement a articles page and model
