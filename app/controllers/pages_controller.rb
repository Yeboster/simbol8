# frozen_string_literal: true

class PagesController < ApplicationController
  def index
    @faker_words = Faker::Lorem.words(200).join ' '
  end
end
