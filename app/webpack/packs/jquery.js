import 'jquery';
window.$ = require('jquery');

// Plugins
import '../vendor/jquery-plugins/jquery.sticky.js';

// Scripts
import sticky from '../scripts/sticky';
import scrollToSecond from '../scripts/scroll';

// Start only when dom is ready
$(document).ready(function() {
  // Stick the icons near the respective text
  sticky();
  // Scroll to second block
  scrollToSecond();
});
