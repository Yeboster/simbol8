function scrollToSecond() {
  $('#main-button').click(function() {
    let secondContainer = document.getElementById("second-block");

    secondContainer.scrollIntoView({
      block: "start",
      behavior: "smooth"
    });
  });
};

export default scrollToSecond;
