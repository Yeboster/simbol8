# frozen_string_literal: true

class AddUsefulInfoToUser < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :username, :string, null: false, default: ''
    add_column :users, :admin, :boolean, default: false
    add_column :users, :image, :string
    add_column :users, :bio, :text

    add_index :users, :username, unique: true
  end
end
