const { dev_server: devServer } = require('@rails/webpacker').config;

const isProduction = process.env.NODE_ENV === 'production';
const inDevServer = process.argv.find(v => v.includes('webpack-dev-server'));
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
  test: /\.vue(\.erb)?$/,
  use: [
    {
      loader: 'vue-loader',
      options: {
        extractCSS: false,
        cssSourceMap: process.env.NODE_ENV !== 'production'
      }
    }
  ]
};
