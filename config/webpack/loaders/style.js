const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
  test: /\.(scss|sass|css)$/i,
  use: [
    // fallback to style-loader in development
    MiniCssExtractPlugin.loader,
    'css-loader',
    'resolve-url-loader',
    {
      loader: 'postcss-loader',
      options: { sourceMap: process.env.NODE_ENV !== 'production' }
    },
    'sass-loader'
  ]
};
