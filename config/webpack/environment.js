const { environment } = require('@rails/webpacker');
const vue = require('./loaders/vue');

const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const style = require('./loaders/style');

const jquery = require('./config/jquery')

environment.plugins.append(
  'MiniCssExtractPlugin',
  new MiniCssExtractPlugin({
    filename: '[name]_[contentHash].css',
    chunkFilename: '[id].css'
    //  sourceMap: environment.
  })
);

environment.loaders.append('sass', style);
environment.loaders.append('vue', vue);

environment.config.merge(jquery);

module.exports = environment;
