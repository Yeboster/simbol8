#!/bin/bash

SESSION_NAME='simbol8'

tmux has-session -t $SESSION_NAME &> /dev/null

if [ $? != 0 ]
then
    tmux new-session -s $SESSION_NAME -d -n 'vim'
    tmux new-window -t $SESSION_NAME:2 -n 'server'
    tmux new-window -t $SESSION_NAME:3 -n 'console'
    tmux new-window -t $SESSION_NAME:4 -n 'git'

    tmux send-keys -t $SESSION_NAME:1 'vi' C-m

    tmux select-window -t $SESSION_NAME:1
fi

tmux attach -t $SESSION_NAME
